
# XMas Projet woche. 

Auf dieser Seite wird eine Liste von **Anwendungen** gezeigt welche man als ICT Fach Person sehr gut gebrauchen kann. 
Auf jedem [Link](##Inhaltsverzeichnis) wird die Anwendung in folgenden Fragen untereilt und jeweils beantwortet. 
* Um was Handelt es sich?
* Warum Git, DevOps, Markdown?
* Welches sind die Vor- und Nachteile?
* Wozu braucht man Git, Devops, Markdown?
* Wie kann ich davon profitieren?
___
## Inhaltsverzeichnis
* [GitHub](#github)
* [GitLab](#gitlab)
* [Markdown]()
* [DevOps]()
[No more Linux]()

### Github 
**Um was Handelt es sich?** 
GitHub ist ein Onlinedienst welche Privat Personen wie auch Firmen ihre Projekte (Hauptsächlich für Software Entwickler) teilen und bearbeiten können. 
GitHub wurde von Chris Wanstrath, PJ Hyett, Scott Chacon und Tom Preston-Werner entwickelt und im Februar 2008 lanciert. Im Oktober „2018 wurde GitHub von Microsoft gekauft. {*blabla} 

**Warum GitHub?**
GitHub ist sehr hilfreich wen man ein Projekt hat welche man mit anderen Teilnehmenden von GitHub, teilen möchte. Mit GitHub ist es möglich, geneinsam in einem Team auf einem selben Projekt zu arbeiten. Der Vorteil ist das die Arbeit aufgeteilt werden kann.

**Welche sind die Vor und Nachteile ?**
*Vorteile* :
* Es hat eine grosse Community
* Jeder Trägt zum Code anderen bei
* Arbeiten in Team
* ...

*Nachteile* :
* Privater Repositories ist Kostenpflichtig
* Nicht  möglich ein anderen VCS zu nutzen das nicht durch GitHub unterstützt wird
*...

### GitLab
**Um was Handelt es sich?**
Gitlab ist ein Version Control System(VCS) und gehört auch wie GitHub zu der Git. Familie. Es bietet fast die gleiche Möglichkeiten wie GitHub. 
GitLab wurde von Dmitri Saparoschez und Valery Sizov im Jahr 2011 entwickelt.

**Warum GitLab?**
GitLab ist wie GitHub es ist für Entwickler und Programmierer interessant und erleichtert stark ihre tägliche Arbeit. Es können mehrere gleichzeitig an einem Projekt arbeiten.


**Welche sind die Vor und Nachteile ?**
*Vorteile* :
* UI sauberer, mobile Ansicht. Es sieht sehr Aufgeräumt aus
* leicht bedienbares Benutzerinterface
* private Repositorys sind kostenlos
* es kann selbst gehostet werden
* ...

*Nachteile* :
* Server sind weniger Stabil
* Es gibt kein Kommentar-Tracking
* Probleme sind nicht in mehreren Repositorys nachverfolgbar
* 

### Markdown
**Um was Handelt es sich?**
Mark down ist eine einfache Auszeichnungssprache die von der Maschine gelesen werden kann.
Mit Hilfe von Zeichen wie # oder () die man in einem Text implementiert, ist es möglich die ganze Formatierung des Textes darzustellen.  Diese Sprache ist sowohl vom Mensch wie auch von der Maschine Lesbar.

*Beispiele von Textgestaltung*:
|              Was             |                                            Wie                                           |
|:----------------------------:|:----------------------------------------------------------------------------------------:|
|         Normaler Text        |                               Wird ohne Zeichen dargestellt                              |
| Kursiv, Fett und Fett kursiv |                      * Kursiv *, ** Fett ** und *** Fett kursiv ***                      |
|         Überschriften        | #Text = Überschift1 ##Text = Überschrift 2 ###Text = Überschrift 3 …. Usw. biss max. 6x# |
|         Bild einfügen        |                             ![Text](link, Pfad, oder bit64..)                            |
|       Horizontale Linie      |                                    --- (3 Bindestrich)                                   |
|       Inline-Quelltext       |                                          ‘text‘                                          |
**Warum Mark down?**
Mark down ist sehr einfach zu bedienen. Und ist sehr hilfreich um schnell zum Beispiel eine *Readme.md* Datei zu erstellen. 

**Welche sind die Vor und Nachteile?**
*Vorteile* : 
* Einfach zu bedienen
* Es ist für den Mensch wie für die Maschine Lesbar
* Keine Programmiersprache Vorkenntnisse
*
*Nachteile*:
* Nicht so viel Möglichkeiten wie mit HTML 
* ...





 